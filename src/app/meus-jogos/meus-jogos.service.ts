import { Injectable } from '@angular/core';
import { HttpClientService } from '../http-client.service';
import { IMeusJogos } from '../models';

@Injectable({
  providedIn: 'root',
})
export class MeusJogosService {
  url = 'api/Jogo';

  constructor(private _httpClientService: HttpClientService) {
  }

  public obterMeusJogos() {
    return this._httpClientService.get<IMeusJogos[]>(`${this.url}/meus-jogos`);
  }
}
