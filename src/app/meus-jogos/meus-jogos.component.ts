import { Component, OnInit } from '@angular/core';
import { IMeusJogos } from '../models';
import { IBolaoModel } from '../models/IBolaoModel';
import { LoaderService } from '../loader/loader.service';
import { BoloesService } from '../boloes/boloes.service';
import { MeusJogosService } from './meus-jogos.service';

@Component({
  selector: 'app-meus-jogos',
  templateUrl: './meus-jogos.component.html',
  styleUrls: ['./meus-jogos.component.less'],
})
export class MeusJogosComponent implements OnInit {
  meusJogos: IMeusJogos[];
  totalPremiosRecebidos = 0;
  bolao: IBolaoModel = {};

  constructor(
    private _loaderService: LoaderService,
    private _meusJogosService: MeusJogosService,
    private _boloesService: BoloesService,
  ) {
  }

  ngOnInit() {
    this.carregarMeusJogos();
  }

  carregarMeusJogos() {
    this._meusJogosService
      .obterMeusJogos()
      .finally(() => this._loaderService.hide())
      .subscribe((meusJogos) => {
        this.meusJogos = meusJogos;
        this.totalPremiosRecebidos = meusJogos.reduce(
          (acc, jogo) => acc + jogo.premioRecebido,
          0
        );
      });
  }

  getJogo(id: number): void {
    this._boloesService.obterBolao(id)
      .finally(() => this._loaderService.hide())
      .subscribe((res) => (this.bolao = res));
  }
}
