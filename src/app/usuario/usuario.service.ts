import { Injectable } from "@angular/core";
import { HttpClientService } from "../http-client.service";
import { throwError } from "rxjs";
import { ICartao } from "../models/ICartao";
import { HttpClient } from "@angular/common/http";
import {
  IDadosBancarios,
  IPerfilUsuario,
  ISexo,
  IUsuarioModel,
  IViaCep,
} from "../models";

@Injectable({
  providedIn: "root",
})
export class UsuarioService {
  url = "api/usuario";

  constructor(
    private _httpClientService: HttpClientService,
    private _http: HttpClient
  ) {}

  public obterUsuarios() {
    return this._httpClientService.get<IUsuarioModel[]>(this.url);
  }

  public alterarSenha(
    id: number,
    senha: { Senha?: string; SenhaConfirmacao?: string; Id?: any }
  ) {
    senha.Id = id;
    return this._httpClientService.put(
      `${this.url}/alterar-senha/${id}`,
      senha
    );
  }

  public obterUsuario(id: number) {
    return this._httpClientService.get<IUsuarioModel>(`${this.url}/${id}`);
  }

  public excluir(id: number) {
    return this._httpClientService.delete(`${this.url}/${id}`);
  }

  public obterPerfis() {
    return this._httpClientService.get<IPerfilUsuario[]>(`${this.url}/perfil`);
  }

  public alterarUsuario(usuarioId: number, usuario: IUsuarioModel) {
    return this._httpClientService.put(`${this.url}/${usuarioId}`, usuario);
  }

  public obterDadosUsuarioLogado() {
    return this._httpClientService.get<IUsuarioModel>(`${this.url}/logado`);
  }

  public buscaCep(cep: string) {
    return this._http.get<IViaCep>(`https://viacep.com.br/ws/${cep}/json/`);
  }

  public obterCartao() {
    return this._httpClientService.get("api/cartao");
  }

  public CadastrarAlterarCartao(tb_cartao: ICartao) {
    return this._httpClientService.post("api/cartao", tb_cartao);
  }

  public obterSexos() {
    return this._httpClientService.get<ISexo[]>(`api/sexo`);
  }

  public obterDadosBancarios() {
    return this._httpClientService.get<IDadosBancarios>(`api/DadoBancario`);
  }

  public cadastrarDadosBancarios(dados: IDadosBancarios) {
    return this._httpClientService.post(`api/DadoBancario`, dados);
  }
}
