import { Component, OnInit } from "@angular/core";
import { UsuarioService } from "../usuario.service";
import { Subject } from "rxjs";
import { ToastrService } from "ngx-toastr";
import { IUsuarioModel } from "src/app/models/IUsuarioModel";
import { IUsuario } from "src/app/models/iUsuario";
import { Router } from "@angular/router";
import { LoaderService } from "src/app/loader/loader.service";

@Component({
  selector: "app-usuario-listar",
  templateUrl: "./usuario-listar.component.html",
  styleUrls: ["./usuario-listar.component.less"],
})
export class UsuarioListarComponent implements OnInit {
  ehExcluir = false;
  constructor(
    private _usuarioService: UsuarioService,
    private _toastrService: ToastrService,
    private _route: Router,
    private _loaderService: LoaderService
  ) {}
  dtOptions: DataTables.Settings = {
    searchPlaceholder: {
      caseInsensitive: true,
      searchPlaceholder: "Pesquisar...",
    },
    language: {
      searchPlaceholder: "Pesquisar...",
      search: "",
      loadingRecords: "Carregando registros",
      processing: "Processando",
      emptyTable: "Nenhum registro foi encotrado",
      zeroRecords: "Não encontrado, tente novamente",
      paginate: {
        first: "Primeiro",
        last: "Ultimo",
        next: "Próximo",
        previous: "Anterior",
      },
      aria: {
        paginate: {
          first: "Primeiro",
          last: "Ultimo",
          next: "Próximo",
          previous: "Anterior",
        },
        sortAscending: "Ascendente",
        sortDescending: "Descendente",
      },
      lengthMenu: "_MENU_",
      info: "Página _PAGE_ de _PAGES_",
      infoFiltered: "(filtrado de _MAX_ de registros)",
    },
  };
  dtTrigger = new Subject();
  usuarios: IUsuarioModel[] = [];
  usuario: IUsuarioModel = {};
  ngOnInit() {
    this.carregarUsuarios(true);
  }
  carregarUsuarios(dtTrig) {
    this._usuarioService
      .obterUsuarios()
      .finally(() => {
        this._loaderService.hide();
      })
      .subscribe(
        (usuarios) => {
          this.usuarios = usuarios;
          console.log(usuarios);
          if (dtTrig) {
            this.dtTrigger.next();
          }
        },
        (error) => {
          if (error.status == 403) {
            this._toastrService.error(
              "Você nao tem autorização para acessar esta tela."
            );
            window.location.href = "/#/home";
          }
        }
      );
  }
  excluir(id) {
    this.usuario = this.usuarios.find((x) => x.id == id);
    this.ehExcluir = true;
  }
  editar(id) {
    this._route.navigate(["/", id, "usuario", "editar", "edt"]);
  }
  detalhar(id) {
    this._route.navigate(["/", id, "usuario", "detalhes", "det"]);
  }
  confirmarExclusao() {
    this._usuarioService
      .excluir(this.usuario.id)
      .finally(() => {
        this._loaderService.hide();
      })
      .subscribe((x) => {
        var resultado = JSON.parse(x._body);
        console.log(resultado);
        if (resultado.sucesso) {
          this._toastrService.success(resultado.message);
          this.carregarUsuarios(false);
        } else {
          this._toastrService.error(resultado.message);
        }
      });
  }
}
