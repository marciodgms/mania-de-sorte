import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, Validators, FormControl } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
//import { CreditCardValidator } from 'ngx2-credit-cards';
//import { Payment } from "payment";

import { LoaderService } from "../../loader/loader.service";
import { UsuarioService } from "../usuario.service";
import { SaquesService } from "../../saques/saques.service";

import {
  ICartao,
  IDadosBancarios,
  IExtrato,
  IPerfilUsuario,
  ISexo,
  IUsuarioModel,
} from "../../models";

@Component({
  selector: "app-usuario-detalhes",
  templateUrl: "./usuario-detalhes.component.html",
  styleUrls: ["./usuario-detalhes.component.less"],
})
export class UsuarioDetalhesComponent implements OnInit {
  cards = [
    {
      type: "amex",
      pattern: /^3[47]/,
      format: /(\d{1,4})(\d{1,6})?(\d{1,5})?/,
      length: [15],
      cvcLength: [4],
      luhn: true,
    },
    {
      type: "dankort",
      pattern: /^5019/,
      format: /(\d{1,4})/g,
      length: [16],
      cvcLength: [3],
      luhn: true,
    },
    {
      type: "dinersclub",
      pattern: /^(36|38|30[0-5])/,
      format: /(\d{1,4})(\d{1,6})?(\d{1,4})?/,
      length: [14],
      cvcLength: [3],
      luhn: true,
    },
    {
      type: "discover",
      pattern: /^(6011|65|64[4-9]|622)/,
      format: /(\d{1,4})/g,
      length: [16],
      cvcLength: [3],
      luhn: true,
    },
    {
      type: "jcb",
      pattern: /^35/,
      format: /(\d{1,4})/g,
      length: [16],
      cvcLength: [3],
      luhn: true,
    },
    {
      type: "laser",
      pattern: /^(6706|6771|6709)/,
      format: /(\d{1,4})/g,
      length: [16, 17, 18, 19],
      cvcLength: [3],
      luhn: true,
    },
    {
      type: "maestro",
      pattern: /^(5018|5020|5038|6304|6703|6708|6759|676[1-3])/,
      format: /(\d{1,4})/g,
      length: [12, 13, 14, 15, 16, 17, 18, 19],
      cvcLength: [3],
      luhn: true,
    },
    {
      type: "mastercard",
      pattern: /^(5[1-5]|677189)|^(222[1-9]|2[3-6]\d{2}|27[0-1]\d|2720)/,
      format: /(\d{1,4})/g,
      length: [16],
      cvcLength: [3],
      luhn: true,
    },
    {
      type: "unionpay",
      pattern: /^62/,
      format: /(\d{1,4})/g,
      length: [16, 17, 18, 19],
      cvcLength: [3],
      luhn: false,
    },
    {
      type: "visaelectron",
      pattern: /^4(026|17500|405|508|844|91[37])/,
      format: /(\d{1,4})/g,
      length: [16],
      cvcLength: [3],
      luhn: true,
    },
    {
      type: "elo",
      pattern:
        /^(4011|438935|45(1416|76|7393)|50(4175|6699|67|90[4-7])|63(6297|6368))/,
      format: /(\d{1,4})/g,
      length: [16],
      cvcLength: [3],
      luhn: true,
    },
    {
      type: "visa",
      pattern: /^4/,
      format: /(\d{1,4})/g,
      length: [13, 16, 19],
      cvcLength: [3],
      luhn: true,
    },
  ];
  senha = { Senha: "", SenhaConfirmacao: "" };
  cartao: ICartao = {};
  tp = "";
  sexos: ISexo[] = [];
  isActiveDiv = true;
  usuario: IUsuarioModel = {
    perfil: {},
    carteira: {},
    endereco: {
      endereco: null,
      uf: null,
      cidade: null,
      logradouro: null,
      numero: null,
      cep: null,
    },
  };
  numCartao = "";
  vencimento = "";
  nomeCartao = "";
  formBuilder: FormBuilder;
  extrato: IExtrato = {
    carteira: {
      id: 0,
      saldo: 0,
      usuarioFk: this.usuario.id,
    },
    dadosBancarios: {},
    transacoes: [],
  };
  dadosBancarios: IDadosBancarios = {
    banco: "",
    agencia: "",
    conta: "",
    contaDigito: "",
    nomeCompleto: "",
    cpf: "",
  };
  usuarioLogado: IUsuarioModel = {
    nome: "",
    sobrenome: "",
    carteira: {},
    perfil: {},
  };
  perfis: IPerfilUsuario[] = [];
  quantidade = 5;
  usuarioAdm = true;

  constructor(
    private _usuarioService: UsuarioService,
    private _saquesService: SaquesService,
    private activeRoute: ActivatedRoute,
    private _router: Router,
    private _loaderService: LoaderService,
    private _toastrService: ToastrService
  ) {}

  ngOnInit() {
    this.activeRoute.params.subscribe((n) => {
      this.usuario.id = n.id;
      this.tp = n.tp;
      switch (this.tp) {
        case "edt":
          this.isActiveDiv = true;
          break;
        case "det":
          this.isActiveDiv = false;
          break;
      }
    });

    this.formBuilder = new FormBuilder();
    this.carregarDados();

    this._usuarioService
      .obterDadosUsuarioLogado()
      .subscribe((user) => (this.usuarioLogado = user));
  }

  verificarCartao() {
    this.cards.forEach((x) => {
      const build = new FormBuilder().group({
        number: new FormControl(
          {
            value: this.numCartao
              .replace(" ", "")
              .replace(" ", "")
              .replace(" ", ""),
          },
          Validators.compose([
            Validators.required,
            Validators.pattern(x.pattern),
          ])
        ),
      });

      if (build.valid) {
        console.log("x.type", x.type);
        console.log("build.valid", build.valid);
        console.log("build.status", build.status);
      }
    });
  }

  carregarDados() {
    this._usuarioService
      .obterCartao()
      .finally(() => this._loaderService.hide())
      .subscribe((cartao) => (this.cartao = cartao));

    this._usuarioService
      .obterPerfis()
      .finally(() => this._loaderService.hide())
      .subscribe((perfis) => (this.perfis = perfis));

    this._usuarioService
      .obterSexos()
      .finally(() => this._loaderService.hide())
      .subscribe((sexos) => (this.sexos = sexos));

    this._usuarioService
      .obterUsuario(this.usuario.id)
      .finally(() => this._loaderService.hide())
      .subscribe((user) => {
        this.usuario = user;
        if (!this.usuario.endereco) {
          this.usuario.endereco = {};
        }

        this.usuarioAdm = this.usuario.perfil.id === 1;

        this._saquesService
          .obterExtratoUsuario(this.usuario.id, this.quantidade)
          .subscribe((extrato) => {
            try {
              if (extrato) {
                this.extrato = extrato;
              }
            } catch (error) {
              console.error("Erro no extrato", error);
            }
          });
      });

    this._usuarioService
      .obterDadosBancarios()
      .subscribe((dadosBancarios) => (this.dadosBancarios = dadosBancarios));
  }

  efetuarSaque() {
    this._saquesService.efetuarSaque().subscribe(
      (res) => this._toastrService.success(res),
      (err) => this._toastrService.error(err._body)
    );
  }

  alterarUsuario() {
    this.usuario.dataNascimento = new Date(this.usuario.dataNascimento);

    this._usuarioService
      .alterarUsuario(this.usuario.id, this.usuario)
      .finally(() => this._loaderService.hide())
      .subscribe(
        (_) => {
          this._usuarioService
            .obterDadosUsuarioLogado()
            .subscribe((user: IUsuarioModel) => {
              if (user.id == this.usuario.id) {
                window.location.href = "/#/";
                this._toastrService.success(
                  "Usuário editado com sucesso, você foi redirecionado para a página de login para que as alterações tenham efeito no sistema."
                );
              } else {
                window.location.href = "/#/usuario/listar";
                this._toastrService.success("Usuário editado com sucesso");
              }
            });
        },
        (err) => {
          this._toastrService.error("Algo deu errado");
          console.error("Algo deu errado", err);
        }
      );
  }

  cadastrarCartao() {
    this._usuarioService
      .CadastrarAlterarCartao(this.cartao)
      .finally(() => this._loaderService.hide())
      .subscribe((_) =>
        this._toastrService.success("Cartão cadastrado com sucesso!")
      );
  }

  enviarDadosBancarios() {
    this._usuarioService
      .cadastrarDadosBancarios(this.dadosBancarios)
      .subscribe((dadosBancarios) => (this.dadosBancarios = dadosBancarios));
  }

  alterarSenha() {
    this._usuarioService
      .alterarSenha(this.usuario.id, this.senha)
      .finally(() => this._loaderService.hide())
      .subscribe(
        (_) => this._toastrService.success("Senha alterada com sucesso"),
        (err) => {
          console.error("Algo deu errado", err);
          this._toastrService.error("Algo deu errado");
        }
      );
  }

  redirecionaPagamentos() {
    this._router.navigateByUrl("pagamento");
  }

  buscaCep() {
    if (this.usuario.endereco.cep) {
      this._loaderService.show();

      this._usuarioService
        .buscaCep(this.usuario.endereco.cep)
        .finally(() => this._loaderService.hide())
        .subscribe(
          (response) => {
            this.usuario.endereco.endereco = response.bairro;
            this.usuario.endereco.uf = response.uf;
            this.usuario.endereco.cidade = response.localidade;
            this.usuario.endereco.logradouro = response.logradouro;
          },
          (error) => {
            console.error("Algo deu errado", error);
            alert("Não foi possivel encontrar os dados do CEP informado.");
            this.usuario.endereco = {};
          }
        );
    }
  }
}
