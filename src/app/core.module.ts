import { CommonModule } from "@angular/common";
import { LoaderComponent } from "./loader/loader.component";
import { LoaderService } from "./loader/loader.service";
import { HttpClientService } from "./http-client.service";
import {
  RequestOptions,
  XHRBackend,
  BaseRequestOptions,
  HttpModule,
  Http
} from "@angular/http";
import { ToastrService } from "ngx-toastr";
import { NgModule } from "@angular/core";

@NgModule({
  imports: [CommonModule, HttpModule],
  exports: [
    LoaderComponent
    // httpServiceFactory
  ],
  declarations: [LoaderComponent],
  providers: [LoaderService, ToastrService]
})
export class CoreModule {}
