import { Component, OnInit, AfterViewInit } from "@angular/core";
import { LoaderService } from "src/app/loader/loader.service";
import { TutoriaisService } from "../tutoriais.service";
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: "app-tutoriais",
  templateUrl: "./tutoriais.component.html",
  styleUrls: ["./tutoriais.component.less"]
})
export class TutoriaisComponent implements OnInit, AfterViewInit {
  public tutoriais: Array<any> = [];
  constructor(
    private _loaderService: LoaderService,
    private _sanitizer:DomSanitizer,
    public tutoriaisService: TutoriaisService
  ) {}

  ngOnInit() {
    this.tutoriaisService.obterTutoriais().subscribe(res => {
      this.tutoriais = res.map(r=>({safeURL:this._sanitizer.bypassSecurityTrustResourceUrl( 'https://www.youtube.com/embed/'+r.link),...r}));
      console.log(this.tutoriais)
    });
  }
  ngAfterViewInit() {
    this._loaderService.hide();
  }
}
