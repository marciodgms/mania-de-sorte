import { Injectable } from "@angular/core";
import { HttpClientService } from "../http-client.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class TutoriaisService {
  url = "api/tutorial";
  constructor(private _httpClientService: HttpClientService) {}

  public obterTutoriais(): Observable<any> {
    return this._httpClientService.get(this.url).map(response => {
      return response;
    });
  }
  public criarTutorial(data): Observable<any> {
    return this._httpClientService.post(this.url,data).map(response => {
      return response;
    });
  }
}
