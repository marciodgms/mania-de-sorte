import { Component, OnInit } from "@angular/core";
import { TutoriaisService } from "../tutoriais.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-tutorial-criar",
  templateUrl: "./tutorial-criar.component.html",
  styleUrls: ["./tutorial-criar.component.less"]
})
export class TutorialCriarComponent implements OnInit {
  tutorial = {
    ordem: 1,
    titulo: "",
    descricao: "",
    link: ""
  };
  constructor(public tutorialService:TutoriaisService,public router:Router) {}

  cadastrarTutorial(f){
    let dados = f.value;
    dados.link = this.tutorial.link.split('v=')[1];
    this.tutorialService.criarTutorial(dados).subscribe(res=>{
      this.router.navigate(['/tutorial'])
    })
  }

  ngOnInit() {}

  cancelar() {}
}
