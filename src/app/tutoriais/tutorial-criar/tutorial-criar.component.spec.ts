import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutorialCriarComponent } from './tutorial-criar.component';

describe('TutorialCriarComponent', () => {
  let component: TutorialCriarComponent;
  let fixture: ComponentFixture<TutorialCriarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutorialCriarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutorialCriarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
