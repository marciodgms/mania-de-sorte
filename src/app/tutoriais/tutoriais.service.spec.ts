import { TestBed } from '@angular/core/testing';

import { TutoriaisService } from './tutoriais.service';

describe('TutoriaisService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TutoriaisService = TestBed.get(TutoriaisService);
    expect(service).toBeTruthy();
  });
});
