import { Injectable } from '@angular/core';
import { HttpClientService } from '../http-client.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {
  url = "api/usuario";
  constructor( private _httpClientService: HttpClientService) { }
  
  public obterDadosUsuarioLogado(): Observable<any> {
    return this._httpClientService
      .get( this.url + "/logado")
      .map((response) => {
        return response;
      }); 
  }
}