import { Component, OnInit } from "@angular/core";
import { HeaderService } from "./header.service";
import { LoaderService } from "../loader/loader.service";
import { IUsuarioModel } from "../models/IUsuarioModel";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.less"],
})
export class HeaderComponent implements OnInit {
  usuarioLogado: IUsuarioModel = {
    nome: "",
    sobrenome: "",
    carteira: {},
    perfil: {},
  };
  constructor(
    private _headerService: HeaderService,
    private _loaderService: LoaderService,
    private _router: Router,
    private _toastrService: ToastrService
  ) {}

  ngOnInit() {
    this._headerService
      .obterDadosUsuarioLogado()
      .finally(() => this._loaderService.hide())
      .subscribe(
        (user) => {
          this.usuarioLogado = user;
          if (user.perfilFk == 2 && user.pagamentos.length == 0) {
            this._router.navigateByUrl("pagamento");
          }
        },
        (error) => {
          if (error.status == 401) {
            this._toastrService.error(
              "Erro de autorização, por favor efetue o login novamente."
            );
            this._router.navigateByUrl("#");
          }
        }
      );
      this.carregaJavascriptPagseguro();
  }

  carregaJavascriptPagseguro() {
    new Promise((resolve) => {
      // let src = 'https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js';
      let src = 'https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js';
      if (document.querySelector(`[src="${src}"]`)) return false;
      let script: HTMLScriptElement = document.createElement('script');
      script.addEventListener('load', r => resolve(null));
      script.src = src;
      document.head.appendChild(script);
    });
  }
}
