import { TestBed } from '@angular/core/testing';

import { PremioSiteService } from './premio-site.service';

describe('PremioSiteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PremioSiteService = TestBed.get(PremioSiteService);
    expect(service).toBeTruthy();
  });
});
