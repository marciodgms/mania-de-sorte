import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';
import { ToastrService } from 'ngx-toastr';
import { IBolaoModel } from '../models/IBolaoModel';
import { IUsuarioModel } from '../models';
import { IPremioSiteModel } from '../models/IPremioSiteModel';
import { HeaderService } from '../header/header.service';
import { LoaderService } from '../loader/loader.service';
import { BoloesService } from '../boloes/boloes.service';
import { PremioSiteService } from './premio-site.service';

@Component({
  selector: 'app-premio-site',
  templateUrl: './premio-site.component.html',
  styleUrls: ['./premio-site.component.less'],
})
export class PremioSiteComponent implements OnInit, AfterViewInit {
  dtOptions: DataTables.Settings = {
    searchPlaceholder: {
      caseInsensitive: true,
      searchPlaceholder: 'Pesquisar...',
    },
    language: {
      searchPlaceholder: 'Pesquisar...',
      search: '',
      loadingRecords: 'Carregando registros',
      processing: 'Processando',
      emptyTable: 'Nenhum registro foi encotrado',
      zeroRecords: 'Não encontrado, tente novamente',
      paginate: {
        first: 'Primeiro',
        last: 'Ultimo',
        next: 'Próximo',
        previous: 'Anterior',
      },
      aria: {
        paginate: {
          first: 'Primeiro',
          last: 'Ultimo',
          next: 'Próximo',
          previous: 'Anterior',
        },
        sortAscending: 'Ascendente',
        sortDescending: 'Descendente',
      },
      lengthMenu: '_MENU_',
      info: 'Página _PAGE_ de _PAGES_',
      infoFiltered: '(filtrado de _MAX_ de registros)',
    },
    order: [[2, 'desc']],
  };
  dtTrigger = new Subject();
  dtTrigger2 = new Subject();
  totalEmPremios = 0;
  premios: IPremioSiteModel[] = [];
  usuario: IPremioSiteModel = {};
  bolao: IBolaoModel = {};

  usuarioLogado: IUsuarioModel = {
    nome: '',
    sobrenome: '',
    carteira: {},
    perfil: {},
  };

  constructor(
    private _loaderService: LoaderService,
    private _headerService: HeaderService,
    private _toastrService: ToastrService,
    private _premioSite: PremioSiteService,
    private _boloesService: BoloesService
  ) {
  }

  ngOnInit() {
    this.carregarPremios(true);
    this._headerService
      .obterDadosUsuarioLogado()
      .finally(() => {
        this._loaderService.hide();
      })
      .subscribe((user) => {
        this.usuarioLogado = user;
        console.log(this.usuarioLogado);
      });
  }

  carregarPremios(doNext) {
    this._premioSite
      .obterPremios()
      .finally(() => this._loaderService.hide())
      .subscribe(
        premios => {
          this.premios = premios;
          this.totalEmPremios = premios.reduce((acc, cur) => acc + cur.premios, 0);

          if (doNext) {
            this.dtTrigger.next();
          }
        },
        (error) => {
          if (error.status === 403) {
            this._toastrService.error(
              'Você nao tem autorização para acessar esta tela.'
            );
            window.location.href = '/#/home';
          } else {
            this._toastrService.error('Algo deu errado');
            console.error('Algo deu errado', error);
          }
        }
      );
  }

  getJogo(id: number) {
    this._boloesService.obterBolao(id)
      .finally(() => this._loaderService.hide())
      .subscribe((res) => (this.bolao = res));
  }

  getPremiosSortedDesc() {
    console.log('this.premios', this.premios);
    const sorted = this.premios.sort((a, b) => b.premios - a.premios);
    console.log('sorted', sorted);
    return sorted;
  }

  ngAfterViewInit() {
    this._loaderService.hide();
  }
}
