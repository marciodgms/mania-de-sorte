import { Injectable } from '@angular/core';
import { HttpClientService } from '../http-client.service';
import { IPremioSiteModel } from '../models/IPremioSiteModel';

@Injectable({
  providedIn: 'root'
})
export class PremioSiteService {
  url = 'api/sorteio/premio-site';

  constructor(private _httpClientService: HttpClientService) {
  }

  public obterPremios() {
    return this._httpClientService.get<IPremioSiteModel[]>(this.url);
  }
}
