import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { DataTablesModule } from "angular-datatables";

import { PremioSiteComponent } from "./premio-site.component";

describe("PremioSiteComponent", () => {
  let component: PremioSiteComponent;
  let fixture: ComponentFixture<PremioSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PremioSiteComponent],
      imports: [DataTablesModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PremioSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
