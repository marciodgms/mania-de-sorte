import { Component, Input } from '@angular/core';
import { IBolaoModel } from '../models/IBolaoModel';

@Component({
  selector: 'app-detalhes-jogo',
  templateUrl: './detalhes-jogo.component.html',
  styleUrls: ['./detalhes-jogo.component.less']
})
export class DetalhesJogoComponent {
  @Input() bolao: IBolaoModel = {};
}
