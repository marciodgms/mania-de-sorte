import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

import { IExtrato, ITransacao } from '../../models';
import { LoaderService } from '../../loader/loader.service';
import { SaquesService } from '../saques.service';

@Component({
  selector: 'app-saques-listar',
  templateUrl: './saques-listar.component.html',
  styleUrls: ['./saques-listar.component.less'],
})
export class SaquesListarComponent implements OnInit {
  dtTrigger = new Subject();
  dtOptions: DataTables.Settings = {
    searchPlaceholder: {
      caseInsensitive: true,
      searchPlaceholder: 'Pesquisar...',
    },
    language: {
      searchPlaceholder: 'Pesquisar...',
      search: '',
      loadingRecords: 'Carregando registros',
      processing: 'Processando',
      emptyTable: 'Nenhum registro foi encotrado',
      zeroRecords: 'Não encontrado, tente novamente',
      paginate: {
        first: 'Primeiro',
        last: 'Ultimo',
        next: 'Próximo',
        previous: 'Anterior',
      },
      aria: {
        paginate: {
          first: 'Primeiro',
          last: 'Ultimo',
          next: 'Próximo',
          previous: 'Anterior',
        },
        sortAscending: 'Ascendente',
        sortDescending: 'Descendente',
      },
      lengthMenu: '_MENU_',
      info: 'Página _PAGE_ de _PAGES_',
      infoFiltered: '(filtrado de _MAX_ de registros)',
    },
  };

  extratos: IExtrato[] = [];

  constructor(
    private _saquesService: SaquesService,
    private _toastrService: ToastrService,
    private _loaderService: LoaderService
  ) {
  }

  ngOnInit() {
    this.carregarSaques(true);
  }

  carregarSaques(doNext: boolean) {
    this._saquesService
      .obterSaquesPendentes()
      .finally(() => this._loaderService.hide())
      .subscribe(
        (extratos) => {
          this.extratos = extratos;

          if (doNext) {
            this.dtTrigger.next();
          }
        },
        (error) => {
          if (error.status === 403) {
            this._toastrService.error(
              'Você nao tem autorização para acessar esta tela.'
            );
            window.location.href = '/#/home';
          } else {
            this._toastrService.error('Algo deu errado');
            console.error('Algo deu errado', error);
          }
        }
      );
  }

  confirmarSaque(transacao: ITransacao) {
    this._saquesService
      .confirmarSaque(transacao)
      .subscribe((_) => this.carregarSaques(false));
  }

  daysSinceRequest(date: Date) {
    const _MS_PER_DAY = 1000 * 60 * 60 * 24;

    const hoje = new Date();

    const utc2 = Date.UTC(hoje.getFullYear(), hoje.getMonth(), hoje.getDate());
    const utc1 = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
  }

  getStyleByDate(date: Date) {
    const days = this.daysSinceRequest(new Date(date));

    let color = 'green';

    if (days >= 8) {
      color = 'red';
    } else if (days >= 3) {
      color = 'orange';
    }

    return {color};
  }
}
