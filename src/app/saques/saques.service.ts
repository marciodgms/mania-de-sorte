import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { HttpClientService } from "../http-client.service";
import { IExtrato } from "../models";
import { ITransacao } from "../models/ITransacao";

@Injectable({
  providedIn: "root",
})
export class SaquesService {
  url = "api/Carteira";

  constructor(private _httpClientService: HttpClientService) {}

  public obterExtratoUsuario(userId: number, qtd: number) {
    return this._httpClientService.get<IExtrato>(
      `${this.url}/${userId}/${qtd}`
    );
  }

  public obterSaquesPendentes() {
    return this._httpClientService.get<IExtrato[]>(
      `${this.url}/pending-withdrawals`
    );
  }

  public obterSaquesSolicitados() {
    return this._httpClientService.get<IExtrato>(
      `${this.url}/requested-withdrawals`
    );
  }

  public confirmarSaque({ id }: ITransacao): Observable<boolean> {
    return this._httpClientService.post(
      `${this.url}/confirm-withdrawal/${id}`,
      {}
    );
  }

  public efetuarSaque() {
    return this._httpClientService.post(`${this.url}/withdraw`, {});
  }
}
