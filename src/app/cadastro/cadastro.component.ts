import { Component, OnInit } from "@angular/core";
import { IUsuario } from "../models/iUsuario";
import { CadastroService } from "./cadastro.service";
import { Router } from "@angular/router";
import { routerNgProbeToken } from "@angular/router/src/router_module";
import { LoaderService } from "../loader/loader.service";

@Component({
  selector: "app-cadastro",
  templateUrl: "./cadastro.component.html",
  styleUrls: ["./cadastro.component.less"]
})
export class CadastroComponent implements OnInit {
  usuario: IUsuario = {};
  lockButton: boolean = true;
  captchaRealizado = false;
  mostraMsg = false;
  error: any;
  lgpdText: string;

  constructor(
    private _route: Router,
    private _cadastroService: CadastroService,
    private _loaderService: LoaderService
  ) {}

  ngOnInit() {
    this._cadastroService.getLgpdText().subscribe(
      (text: string) => {
        this.lgpdText = text;
      }
    );
  }

  cadastrar() {
    if (this.captchaRealizado) {
      this.mostraMsg = false;
      this._cadastroService.cadastrarUsuario(this.usuario).subscribe(
        x => {
          console.log(x);
          this._route.navigate(["login"]);
        },
        err => {
          this.error = err._body;
        },
        () => {
          console.log("finalizou");
        }
      );
    } else {
      this.mostraMsg = true;
    }
  }

  resolved(token: string) {
    this.captchaRealizado = !!token;
  }
}
