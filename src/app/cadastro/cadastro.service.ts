import { Injectable } from "@angular/core";
import { HttpClientService } from "../http-client.service";
import { Observable } from "rxjs";
import { IUsuario } from "../models/iUsuario";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";
@Injectable({
  providedIn: "root"
})
export class CadastroService {
  url = "api/usuario";
  constructor(private _httpClientService: HttpClientService) {}

  public cadastrarUsuario(usuario: IUsuario): Observable<any> {
    return this._httpClientService
      .post(this.url, usuario)
      .map((response: any) => {
        console.log(response);
        return response._body;
      });
  }

  public getLgpdText() {
    return this._httpClientService.get('api/Usuario/obter-texto-lgpd');
  }
}
