import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientService } from '../http-client.service';
import { IBolao, IBoloesModel, ICarteira } from '../models';
import { IJogoModel } from '../models/IJogoModel';
import { IBolaoModel } from '../models/IBolaoModel';

@Injectable({
  providedIn: 'root',
})
export class BoloesService {
  url = 'api/bolao';
  urlPag = 'api/pagamento';
  urlUsu = 'api/carteira';

  constructor(private _httpClientService: HttpClientService) {
  }

  public obterBolao(id: number) {
    const countOccurrences = (arr, val) => arr.reduce((a, v) => (v === val ? a + 1 : a), 0);

    return this._httpClientService.get<IBolao>(`${this.url}/${id}`)
      .map(bolao => {
        const dezenasSorteadas = bolao.dezenasSorteadas.split(';');

        const jogos = bolao.jogos.map((jogo) => {
          const numeros = jogo.numeros.split(';')
            .map((num) => (`00${num}`).slice(-2));

          const acertos = dezenasSorteadas.reduce(
            (acc, dezena) => acc + countOccurrences(numeros, dezena),
            0
          );

          const jogoModel: IJogoModel = {
            id: jogo.id,
            numeros,
            acertos,
          };
          return jogoModel;
        });

        const bolaoModel: IBolaoModel = {...bolao, dezenasSorteadas, jogos};
        return bolaoModel;
      });
  }

  public obterBoloes() {
    return this._httpClientService.get<IBoloesModel[]>(this.url);
  }

  public jogarAgora(data: FormData) {
    return this._httpClientService.post(`${this.url}/jogar-bolao`, data);
  }

  public criarBolao(data: FormData) {
    return this._httpClientService.post(`${this.url}/criar-bolao`, data);
  }

  public obterAutenticaBolao(valor: number): Observable<any> {
    return this._httpClientService.get(`${this.urlPag}/autentica-bolao/${valor}`);
  }

  public carteira(): Observable<any> {
    return this._httpClientService.get<ICarteira>(`${this.urlUsu}/wallet`);
  }
}
