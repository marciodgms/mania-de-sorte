declare var PagSeguroLightbox: any;

import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import { objectToFormData } from '../../util/util';
import { IBoloesModel } from '../../models';
import { IImagem } from '../../models/IImagem';
import { LoaderService } from '../../loader/loader.service';
import { MeusJogosService } from '../../meus-jogos/meus-jogos.service';
import { BoloesService } from '../boloes.service';
import { IBolaoModel } from '../../models/IBolaoModel';
import { ICarteira } from "src/app/models";

@Component({
  selector: 'app-bolao-listar',
  templateUrl: './bolao-listar.component.html',
  styleUrls: ['./bolao-listar.component.less'],
})
export class BolaoListarComponent implements OnInit {
  hashCode: any;
  @ViewChild('closebutton') closebutton;

  apiUrl = environment.apiUrl;
  painelImageUrl = 'https://painel.maniadesorte.com.br/MDS/Upload/';

  bolao: IBolaoModel = {};
  boloes: IBoloesModel[] = [];
  bolaoSelecionado: IBoloesModel;
  carteira: ICarteira = {
    saldo: 0
  };

  imagens?: IImagem[];
  qtdCotas = 1;
  meioDePagamento: 0;
  valorPagamento: string;

  slideConfig = {
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000
  };

  constructor(
    private _boloesService: BoloesService,
    private _loaderService: LoaderService,
    private _toastrService: ToastrService,
    private _meusJogosService: MeusJogosService
  ) {
  }

  ngOnInit() {
    this.carregarBoloes();
    console.log(this);
  }

  carregarBoloes() {
    this._boloesService
      .obterBoloes()
      .finally(() => this._loaderService.hide())
      .subscribe((boloes) => (this.boloes = boloes));
  }

  verificarMaximo() {
    if (this.bolaoSelecionado) {
      const {quantidadeCotas} = this.bolaoSelecionado.bolao;

      if (this.qtdCotas > quantidadeCotas) {
        this.qtdCotas = quantidadeCotas;
      } else if (this.qtdCotas < 1) {
        this.qtdCotas = 1;
      }
    }
  }

  selecionarBolao(bolao) {
    this.bolaoSelecionado = bolao;
    this._boloesService
      .carteira()
      .subscribe(response => {
        this.carteira = response;
      });
  }

  jogarAgora() {
    if (!this.meioDePagamento || this.meioDePagamento === 0) {
      this._toastrService.error('Favor selecionar um meio de pagamento');
      return;
    }

    this.meioDePagamento == 2 ? this.autenticaPagmentoBolao() : this.jogar('saldo');
  }

  autenticaPagmentoBolao() {
    let valor = this.bolaoSelecionado.bolao.valorCota * this.qtdCotas;

    this._boloesService
      .obterAutenticaBolao(valor)
      .finally(() => this._loaderService.hide())
      .subscribe(response => {
        this.hashCode = response.token;
        this.abrirPagSeguro(response.redirect);
      });
  }

  abrirPagSeguro(redirect) {
    if (typeof PagSeguroLightbox == 'function') {
      var isOpenLightbox = PagSeguroLightbox(this.hashCode, {
        success: (transactionCode) => {
          this.jogar(transactionCode);
        },
        abort: (mensagem) => {
          this._toastrService.error("Não foi possível efetuar o jogo devido o erro do pagamento: " + mensagem);
        }
      });

      if (!isOpenLightbox) {
        location.href = redirect;
      }
    } else {
      setTimeout(() => {
        this.abrirPagSeguro(redirect);
      }, 300);
    }
  }

  jogar(transactionCode) {
    const formData = objectToFormData({
      idBolao: this.bolaoSelecionado.bolao.id,
      quantidade: this.qtdCotas,
      meioDePagamento: this.meioDePagamento,
      codigoConfirmacao: transactionCode
    });

    this._boloesService
      .jogarAgora(formData)
      .finally(() => this._loaderService.hide())
      .subscribe(
        (res) => {
          this._toastrService.success('Cotas adquiridas com sucesso!');
          this.closebutton.nativeElement.click();

          this.carregarBoloes();
        },
        (err) => this._toastrService.error(err.json())
      );
  }

  esgotado() {
    this._toastrService.info(
      'A quantidade de cotas máxima para este bolão foi atingida.'
    );
  }

  setImageUrlFromPainel(e, imagem: IImagem): void {
    e.target.src = `${this.painelImageUrl}${imagem.hash_arquivo}`;
  }

  getJogo(bolao: any): void {
    this._boloesService.obterBolao(bolao.bolao.id)
      .finally(() => this._loaderService.hide())
      .subscribe(res => (this.bolao = res));
  }
}
