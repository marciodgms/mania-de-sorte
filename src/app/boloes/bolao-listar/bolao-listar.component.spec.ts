import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BolaoListarComponent } from './bolao-listar.component';

describe('BolaoListarComponent', () => {
  let component: BolaoListarComponent;
  let fixture: ComponentFixture<BolaoListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BolaoListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BolaoListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
