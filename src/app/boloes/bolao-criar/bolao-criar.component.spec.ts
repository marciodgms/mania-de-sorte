import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BolaoCriarComponent } from './bolao-criar.component';

describe('BolaoCriarComponent', () => {
  let component: BolaoCriarComponent;
  let fixture: ComponentFixture<BolaoCriarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BolaoCriarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BolaoCriarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
