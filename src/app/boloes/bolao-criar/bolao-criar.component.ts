import { Component, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { objectToFormData } from "src/app/util/util";
import { LoaderService } from "../../loader/loader.service";
import { ICriarBolao } from "../../models/ICriarBolao";
import { BoloesService } from "../boloes.service";

@Component({
  selector: "app-bolao-criar",
  templateUrl: "./bolao-criar.component.html",
  styleUrls: ["./bolao-criar.component.less"],
})
export class BolaoCriarComponent implements OnInit {
  criarBolao: ICriarBolao = {
    jogos: [],
    imagens: [],
  };
  imagesUrls: string[] = [];

  showAlert = false;
  showMessage = false;

  constructor(
    private _toastrService: ToastrService,
    private _loaderService: LoaderService,
    public boloesService: BoloesService
  ) {}

  ngOnInit() {
    this._loaderService.hide();
  }
  selecionarDezenas() {
    this.criarBolao.jogos.push("");
  }
  confirmar() {
    const formData = objectToFormData(this.criarBolao);

    for (const image of this.criarBolao.imagens) {
      formData.append("imagens", image);
    }

    this.boloesService.criarBolao(formData).subscribe(
      (res) => {
        this.showAlert = true;
        this._toastrService.success(res.titulo);

        this.criarBolao = {
          jogos: [],
          imagens: [],
        };

        setTimeout(() => (this.showAlert = false), 3000);
      },
      (er) => this._toastrService.error(er.json())
    );
  }
  removerIndex(index) {
    this.criarBolao.jogos.splice(index, 1);
  }
  cancelar() {
    this.criarBolao = {
      jogos: [],
      imagens: [],
    };
  }
  preview(files: FileList) {
    if (files.length === 0) return;

    const images = Array.from(files);

    this.showMessage = images.some((f) => f.type.match(/image\/*/) == null);
    if (this.showMessage) {
      return;
    }

    for (const image of images) {
      const reader = new FileReader();

      reader.readAsDataURL(image);

      reader.onload = (_event) =>
        this.imagesUrls.push(reader.result.toString());
    }

    this.criarBolao.imagens = images;
  }
}
