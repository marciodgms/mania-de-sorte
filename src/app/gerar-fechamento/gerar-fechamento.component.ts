import {AfterViewInit, Component, OnInit} from '@angular/core';
import {KeyValue, Location} from '@angular/common';
import {finalize} from 'rxjs/operators';
import {NgxSpinnerService} from 'ngx-spinner';
import {jsPDF} from 'jspdf';
import html2canvas from 'html2canvas';
import {LoaderService} from '../loader/loader.service';
import {ITipoFechamento} from '../models/ITipoFechamento';
import {IFechamento} from '../models/IFechamento';
import {IFechamentoCompleto} from '../models/IFechamentoGerado';
import {GerarFechamentoService} from './gerar-fechamento.service';

@Component({
  selector: 'app-gerar-fechamento',
  templateUrl: './gerar-fechamento.component.html',
  styleUrls: ['./gerar-fechamento.component.less'],
})
export class GerarFechamentoComponent implements OnInit, AfterViewInit {
  mtrzJogo: any;
  quantidadeNumeros = 0;
  numeros = Array.from(Array(25).keys()).map(x => x + 1);
  numerosTotais = Array.from(Array(25).keys()).map(x => x + 1);

  fechamentosGerados: IFechamentoCompleto = null;
  fechamentos: IFechamento[] = [];
  tipoFechamentoId: number;
  tiposFechamentos: ITipoFechamento[];

  acertosPremiados: { '15': number, '14': number, '13': number, '12': number, '11': number };
  resultadoConcurso = '';
  concursos: any;

  teimosinha: { Id: any; Numero: any };
  teimosinhas: any[] = [];

  errorMsg: string;

  constructor(
    private _loaderService: LoaderService,
    private _location: Location,
    private spinner: NgxSpinnerService,
    public gerarFechamentoService: GerarFechamentoService
  ) {
  }

  ngOnInit() {
    this.obterTiposFechamentos();

    this.gerarFechamentoService
      .obterTeimosinhas()
      .subscribe(teimosinhas => this.teimosinhas = teimosinhas);

    this.gerarFechamentoService
      .obterConcursos()
      .subscribe(concursos => (this.concursos = concursos));
  }

  ngAfterViewInit() {
    this._loaderService.hide();
  }

  obterTiposFechamentos() {
    this.mtrzJogo = undefined;
    this.limparSelecionados();

    this.gerarFechamentoService
      .obterTiposFechamentos()
      .subscribe((res: ITipoFechamento[]) => (this.tiposFechamentos = res));
  }

  buscarFechamentos() {
    this.mtrzJogo = undefined;
    this.limparSelecionados();

    if (!this.tipoFechamentoId) {
      this.fechamentos = [];
      return;
    }

    this.gerarFechamentoService
      .obterFechamentos(this.tipoFechamentoId)
      .subscribe((res: IFechamento[]) => {
        this.fechamentos = res.filter(
          (f) => f.qtdDezenas === this.quantidadeNumeros
        );
      });
  }

  mudouQtd() {
    const elements = document.getElementsByClassName('selecionado');

    this.buscarFechamentos();

    for (let i = elements.length - 1; i >= 0; i--) {
      const element = elements.item(i);

      if (
        document.getElementsByClassName('selecionado').length >
        this.quantidadeNumeros
      ) {
        element.classList.remove('selecionado');
      }
    }
  }

  palpite() {
    this.limparSelecionados();

    const numerosRandom: number[] = [];
    while (numerosRandom.length < this.quantidadeNumeros) {
      const indexRandom = Math.floor(Math.random() * this.numerosTotais.length);

      if (!numerosRandom.some((e) => e === this.numerosTotais[indexRandom])) {
        numerosRandom.push(this.numerosTotais[indexRandom]);

        const element = document.getElementById(
          (this.numerosTotais[indexRandom] - 1).toString()
        );
        element.classList.add('selecionado');
      }
    }

    const fixosRandom: number[] = [];
    while (fixosRandom.length < this.mtrzJogo.qtdFixas) {
      const indexRandom = Math.floor(Math.random() * this.numerosTotais.length);

      const element = document.getElementById(
        (this.numerosTotais[indexRandom] - 1).toString()
      );

      if (
        !fixosRandom.some((e) => e === this.numerosTotais[indexRandom]) &&
        element.classList.contains('selecionado')
      ) {
        fixosRandom.push(this.numerosTotais[indexRandom]);
        element.classList.add('double-selecionado');
      }
    }
  }

  limparSelecionados() {
    Array.from(document.getElementsByClassName('numeroJogo')).forEach(
      (element) => {
        element.classList.remove('double-selecionado');
        element.classList.remove('selecionado');
      }
    );
  }

  onmtrzJogoChange(mtrzJogo) {
    this.mtrzJogo = mtrzJogo;
  }

  selecionarNumero(id) {
    if (!this.mtrzJogo) {
      return;
    }

    const numeroSelecionado = document.getElementById(id);

    const qtdDoubleSelecionados = document.getElementsByClassName('double-selecionado').length;
    const qtdSelecionados = document.getElementsByClassName('selecionado').length;

    if (numeroSelecionado.classList.contains('double-selecionado')) {
      numeroSelecionado.classList.remove('selecionado');
      numeroSelecionado.classList.remove('double-selecionado');

      return;
    }

    if (numeroSelecionado.classList.contains('selecionado')) {
      if (qtdDoubleSelecionados < this.mtrzJogo.qtdFixas) {
        numeroSelecionado.classList.add('double-selecionado');
      } else {
        numeroSelecionado.classList.remove('selecionado');
      }

      return;
    }

    if (qtdSelecionados < this.quantidadeNumeros) {
      numeroSelecionado.classList.add('selecionado');
    }
  }

  selecionarTeimosinha(el: HTMLElement): void {
    const selecionados = el
      .closest('.teimosinhas')
      .getElementsByClassName('selecionado');

    Object.values(selecionados).map((child) =>
      child.classList.remove('selecionado')
    );

    el.classList.add('selecionado');
  }

  deselecionarTeimosinha(): void {
    const teimosinhas = document.querySelector('.teimosinhas');

    if (teimosinhas) {
      const selecionados = teimosinhas.getElementsByClassName('selecionado');

      Object.values(selecionados).map((child) =>
        child.classList.remove('selecionado')
      );

      this.teimosinha = null;
    }
  }

  voltar() {
    this._location.back();
  }

  podeGerarFechamento() {
    const qtdSelecionados = document.getElementsByClassName('selecionado').length;
    return qtdSelecionados === this.quantidadeNumeros;
  }

  handleGerarFechamento() {
    const fixasHTML = Array.from(
      document.getElementsByClassName('double-selecionado'),
      (n) => n.textContent
    );
    const fixas = fixasHTML.join(';');

    const numerosHTML = Array.from(
      document.getElementsByClassName('selecionado'),
      (n) => n.textContent
    );
    const numeros = numerosHTML
      .filter((n) => !fixasHTML.some((f) => f === n))
      .join(';');

    this.gerarFechamentoService
      .criarFechamento(this.mtrzJogo.id, {fixas, numeros})
      .subscribe(
        (res) => {
          this.fechamentosGerados = {
            matriz: res.matriz,
            fechamentos: res.numeros.map(n => ({numeros: n})),
          };
        },
        (err) => (this.errorMsg = err._body)
      );
  }

  handleChangeConcurso() {
    this.acertosPremiados = {15: 0, 14: 0, 13: 0, 12: 0, 11: 0};

    for (let i = 0; i < this.fechamentosGerados.fechamentos.length; i++) {
      const fechamento = this.fechamentosGerados.fechamentos[i];

      const numAcertos = this.getNumAcertos(fechamento.numeros);
      this.fechamentosGerados.fechamentos[i].acertos = numAcertos;

      if (numAcertos in this.acertosPremiados) {
        this.acertosPremiados[numAcertos]++;
      }
    }
  }

  getAcertos(fechamento: string) {
    const dezenasConcurso = this.resultadoConcurso
      .split(';').map((r) => +r);

    const numeros = fechamento.split(';').map((f) => +f);
    return numeros.filter((n) => dezenasConcurso.includes(n));
  }

  getNumAcertos(fechamento: string) {
    const acertos = this.getAcertos(fechamento);
    return acertos.length;
  }

  download() {
    this.spinner.show();

    const listaJogos = this.fechamentosGerados.fechamentos.map(f => f.numeros);
    const listaAcertos = this.fechamentosGerados.fechamentos.map((f) => f.acertos);

    this.gerarFechamentoService
      .obterUrlPdfDownload({
        idFechamento: this.mtrzJogo.id,
        listaJogos,
        listaAcertos,
      })
      .pipe(finalize(() => this.spinner.hide()))
      .subscribe(
        (res) => {
          const a = document.createElement('a');
          a.href = res.text();
          a.setAttribute('target', '_blank');
          a.click();
        },
        (err) => (this.errorMsg = err.message)
      );
  }

  toggleNumAcertos(visible: boolean) {
    const numAcertosDivs = document.querySelectorAll('.num-acertos');

    [].forEach.call(
      numAcertosDivs,
      (el: HTMLElement) => (el.style.visibility = visible ? '' : 'hidden')
    );
  }

  generateFilename(ext = 'pdf') {
    const now = new Date();
    const ano = now.getFullYear();
    const mes = `${now.getMonth() + 1}`.padStart(2, '0');
    const dia = `${now.getDay()}`.padStart(2, '0');
    const hh = `${now.getHours()}`.padStart(2, '0');
    const mm = `${now.getMinutes()}`.padStart(2, '0');
    const ss = `${now.getSeconds()}`.padStart(2, '0');

    return `fechamento_${ano}${mes}${dia}${hh}${mm}${ss}.${ext}`;
  }

  download2() {
    this.spinner.show();

    const jogo = document.getElementsByClassName('box-jogo')[0];
    const h3Header = jogo.children[0].children[0].textContent;
    const rowFechamentos = jogo.children[1] as HTMLElement;

    this.toggleNumAcertos(false);

    html2canvas(rowFechamentos, {
      scale: window.devicePixelRatio,
      scrollX: 0,
      scrollY: -window.scrollY,
    })
      .then((canvas) => {
        const imgWidth = 190;
        const pageHeight = 290;
        const imgHeight = ((canvas.height * imgWidth) / canvas.width) * 1.2;

        const imgData = canvas.toDataURL('image/png');
        const doc = new jsPDF('p', 'mm', 'a4');

        const splittedTitle = doc.splitTextToSize(h3Header, 200);
        doc.text(splittedTitle, 5, 10);

        let position = 20;
        doc.addImage(imgData, 'PNG', 5, position, imgWidth, imgHeight);

        let heightLeft = imgHeight - pageHeight;
        while (heightLeft >= 0) {
          position = heightLeft - imgHeight;
          doc.addPage();
          doc.addImage(imgData, 'PNG', 5, position, imgWidth, imgHeight);
          heightLeft -= pageHeight;
        }

        const filename = this.generateFilename();
        doc.save(filename);
      })
      .finally(() => {
        this.toggleNumAcertos(true);
        this.spinner.hide();
      });
  }

  imprimir(idTeimosinha?: string) {
    this.spinner.show();

    this.gerarFechamentoService
      .obterUrlPdfImpressao(this.fechamentosGerados.fechamentos.map(f => f.numeros), idTeimosinha)
      .pipe(
        finalize(() => {
          this.deselecionarTeimosinha();
          this.spinner.hide();
        })
      )
      .subscribe(
        (res) => {
          const a = document.createElement('a');
          a.href = res.text();
          a.setAttribute('target', '_blank');
          a.click();
        },
        (err) => (this.errorMsg = err.message)
      );
  }

  keyDescOrder(a: KeyValue<number, number>, b: KeyValue<number, number>) {
    return a.key > b.key ? -1 : (b.key > a.key ? 1 : 0);
  }
}
