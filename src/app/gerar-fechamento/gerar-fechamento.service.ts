import { Injectable } from '@angular/core';
import { HttpClientService } from '../http-client.service';
import { Http } from '@angular/http';
import { map, finalize } from 'rxjs/operators';
import { LoaderService } from '../loader/loader.service';
import { IFechamentoGerado } from '../models/IFechamentoGerado';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GerarFechamentoService {
  private painelUrl = 'https://painel.maniadesorte.com.br/MDS';

  constructor(
    private httpClientService: HttpClientService,
    private http: Http,
    private loaderService: LoaderService
  ) {}

  obterTiposFechamentos() {
    return this.httpClientService.get('api/fechamento/tipos');
  }

  obterFechamentos(tipoFechamentoId: number) {
    return this.httpClientService.get(`api/fechamento/${tipoFechamentoId}`);
  }

  criarFechamento(id, dados: { numeros: string; fixas: string }): Observable<IFechamentoGerado> {
    return this.httpClientService.post(`api/fechamento/${id}`, dados);
  }

  private buildQueryString(params) {
    return [
      'sys=MDS',
      ...Object.entries(params).map(([key, value]) => `${key}=${value}`),
    ].join('&');
  }

  async get(resource: string, params: any) {
    this.loaderService.show();

    const queryString = this.buildQueryString(params);
    const url = `${this.painelUrl}/${resource}?${queryString}`;

    return await fetch(url, { method: 'GET' }).finally(() =>
      this.loaderService.hide()
    );
  }

  private post(resource: string, data: FormData) {
    this.loaderService.show();

    return this.http
      .post(`${this.painelUrl}/${resource}`, data)
      .pipe(finalize(() => this.loaderService.hide()));
  }

  obterUrlPdfDownload({
    idFechamento,
    listaJogos,
    listaAcertos,
  }: {
    idFechamento: string;
    listaJogos: string[];
    listaAcertos: number[];
  }) {
    const data = new FormData();
    data.append('sys', 'MDS');
    data.append('idFechamento', idFechamento);
    data.append('listaJogos', JSON.stringify(listaJogos));
    data.append('listaAcertos', JSON.stringify(listaAcertos));

    return this.post('BaixarRelatorioFechamento.rule', data);
  }

  obterUrlPdfImpressao(lista: string[], idTeimosinha?: string) {
    const data = new FormData();
    data.append('sys', 'MDS');
    data.append('lista', JSON.stringify(lista));

    if (idTeimosinha) {
      data.append('idTeimosinha', idTeimosinha);
    }

    return this.post('GravarJogos.rule', data);
  }

  obterTeimosinhas() {
    this.loaderService.show();

    return this.http
      .get(`${this.painelUrl}/ApiTeimosinha.rule?sys=MDS`)
      .pipe(
        map((res: any) =>
          res && res._body && res._body.indexOf('{') !== -1
            ? JSON.parse(`[${res._body}]`)
            : []
        ),
        finalize(() => this.loaderService.hide())
      );
  }

  obterConcursos() {
    return this.httpClientService.get(`api/sorteio/concursos`);
  }
}
