import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { PagamentoComponent } from './pagamento/pagamento.component';
import { NgModule } from '@angular/core';
import { CadastroComponent } from './cadastro/cadastro.component';
import { RecuperarSenhaComponent } from './login/recuperar-senha/recuperar-senha.component';
import { GerarFechamentoComponent } from './gerar-fechamento/gerar-fechamento.component';
import { UsuarioDetalhesComponent } from './usuario/usuario-detalhes/usuario-detalhes.component';
import { BolaoListarComponent } from './boloes/bolao-listar/bolao-listar.component';
import { UsuarioListarComponent } from './usuario/usuario-listar/usuario-listar.component';
import { SaquesListarComponent } from './saques/saques-listar/saques-listar.component';
import { TutoriaisComponent } from './tutoriais/tutoriais/tutoriais.component';
import { MeusJogosComponent } from './meus-jogos/meus-jogos.component';
import { BolaoCriarComponent } from './boloes/bolao-criar/bolao-criar.component';
import { PremioSiteComponent } from './premio-site/premio-site.component';
import { TutorialCriarComponent } from './tutoriais/tutorial-criar/tutorial-criar.component';
import {DetalhesJogoComponent} from './detalhes-jogo/detalhes-jogo.component';

const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'cadastro',
    component: CadastroComponent
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'pagamento',
    component: PagamentoComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'recuperar-senha',
    component: RecuperarSenhaComponent
  },
  {
    path: 'gerar-fechamento',
    component: GerarFechamentoComponent
  },
  {
    path: 'bolao/listar',
    component: BolaoListarComponent
  },
  {
    path: 'bolao/cadastrar',
    component: BolaoCriarComponent
  },
  {
    path: 'usuario/listar',
    component: UsuarioListarComponent
  },
  {
    path: 'saques/listar',
    component: SaquesListarComponent
  },
  {
    path: 'usuario/listar',
    component: UsuarioListarComponent
  },
  {
    path: ':id/usuario/detalhes/:tp',
    component: UsuarioDetalhesComponent,
    data: {
      hide: true
    }
  },
  {
    path: ':id/usuario/editar/:tp',
    component: UsuarioDetalhesComponent,
    data: {
      hide: true
    }
  },
  {
    path: 'tutorial',
    component: TutoriaisComponent,
    data: {
      hide: true
    }
  },
  {
    path: 'tutorial/criar',
    component: TutorialCriarComponent,
    data: {
      hide: true
    }
  },
  {
    path: 'meus-jogos',
    component: MeusJogosComponent,
    data: {
      hide: true
    }
  },
  {
    path: 'detalhes-jogo',
    component: DetalhesJogoComponent,
    data: {
      hide: true
    }
  },
  {
    path: 'premio-site',
    component: PremioSiteComponent,
    data: {
      hide: true
    }
  }
];
@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
