export interface ICartao{
    id?: number;
    nomeCartao?: string;
    dataExp?: string;
    cvv?: string;
    numero?: string;
    descricao?: string;
    usuarioFk?: number;
    bandeira?: string;
}