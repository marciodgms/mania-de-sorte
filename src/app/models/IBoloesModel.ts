import { IBolao } from './IBolao';
import { IJogo } from './IJogo';
import { IImagem } from './IImagem';

export interface IBoloesModel {
  bolao?: IBolao;
  jogos?: IJogo[];
  imagens?: IImagem[];
  dtTotal?: number;
  dtPercorrido?: number;
  porcentagem?: number;
}
