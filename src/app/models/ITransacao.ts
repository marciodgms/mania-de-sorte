export interface ITransacao {
  id?: number;
  valor?: number;
  dataSolicitacao?: Date;
  tipoTransacao?: "Entrada" | "Saida";
}
