export interface IPremioSiteModel {
  id?: number;
  premios?: number;
  concurso?: string;
  data?: Date;
  grupo?: string;
  cotas?: number;
  quantidade15?: string;
  quantidade14?: string;
  quantidade13?: string;
  quantidade12?: string;
  quantidade11?: string;
}

export interface IPremioSiteEstadoModel {
  quantidade15?: number;
  quantidade14?: number;
  quantidade13?: number;
  quantidade12?: number;
  quantidade11?: number;
  premios?: number;
  cotas?: number;
  concurso?: string;
  data?: Date;
  estado?: string;
  QuantidadeEstado?: number;
}
