import { IJogo } from './IJogo';

export interface IBolao {
  id?: number;
  titulo?: string;
  numeroConcurso?: number;
  dataBolao?: Date;
  dataCadastro?: Date;
  quantidadeCotas?: number;
  valorCota?: number;
  valorSorteio?: number;
  qtdCotasVendidas?: number;
  dezenasSorteadas?: string;
  detalhes?: string;

  jogos?: IJogo[];
}
