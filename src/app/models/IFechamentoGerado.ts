export interface IFechamentoGerado {
  matriz: string;
  numeros: string[];
}

export interface IFechamentoCompleto {
  matriz: string;
  fechamentos: {
    numeros: string;
    acertos?: number;
  }[];
}
