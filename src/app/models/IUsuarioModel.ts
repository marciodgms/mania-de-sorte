import { IAddress, ICarteira, IPerfilUsuario, IPagamento, IUsuario } from ".";

export interface IUsuarioModel extends IUsuario {
  foto?: any;
  status?: boolean;
  qntBoloes?: number;
  perfil?: IPerfilUsuario;
  carteira?: ICarteira;
  endereco?: IAddress;
  tempoPermanencia?: number;
  premioRecebido?: number;
  qntCotas?: number;
  pagamento?: IPagamento;
}
