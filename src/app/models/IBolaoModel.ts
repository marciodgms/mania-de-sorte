import { Omit } from '../util/util';
import { IJogoModel } from './IJogoModel';
import { IBolao } from './IBolao';

export interface IBolaoModel extends Omit<IBolao, 'dezenasSorteadas' | 'jogos'> {
  dezenasSorteadas?: string[];
  jogos?: IJogoModel[];
}
