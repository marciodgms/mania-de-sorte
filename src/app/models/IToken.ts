export interface IToken{
    authenticated: boolean;
    created: Date;
    expiration: Date;
    acessToken: string;
}