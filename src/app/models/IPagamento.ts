import { IUsuario } from "./iUsuario";

export interface IPagamento {
    id?:number;
    usuario?:IUsuario;
    usuarioFK?:number;
    pagamentoSituacaoFK?:number;
    dataCadastro?:Date;
    dataVigencia?:Date;
    valorPago?:number;
    codigoAutenticacao?: string;
    codigoConfirmacao?: string;
    observacao?:string;
}