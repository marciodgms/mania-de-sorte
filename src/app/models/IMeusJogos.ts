export interface IMeusJogos {
  id?: number;
  premioRecebido?: number;
  dataSorteio?: number;
  concurso?: number;
  grupo?: number;
  cotasAdquiridas?: number;
  totalDeCotas?: number;
  premioTotalDoBolao?: number;
  quantidade11?: number;
  quantidade12?: number;
  quantidade13?: number;
  quantidade14?: number;
  quantidade15?: number;
}
