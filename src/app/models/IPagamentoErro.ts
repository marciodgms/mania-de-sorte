export interface IPagamentoErro{
    CodigoAutenticacao?: string;
    Mensagem?: string;
}