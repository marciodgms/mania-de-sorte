export interface IUsuario {
  id?: number;
  nome?: string;
  sobrenome?: string;
  email?: string;
  cpf?: string;
  dataNascimento?: Date;
  sexoFK?: number;
  telefone?: string;
  enderecoFK?: number;
  senha?: string;
  perfilFk?: number;
  receberemail?: boolean;
}
