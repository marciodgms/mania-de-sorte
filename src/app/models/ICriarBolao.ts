import { IJogo } from "./IJogo";

export interface ICriarBolao {
  titulo?: string;
  numeroConcurso?: number;
  dataBolao?: Date;
  quantidadeCotas?: number;
  valorCota?: number;
  jogos?: string[];
  imagens?: File[];
}
