export interface IViaCep {
  bairro?: string;
  uf?: string;
  localidade?: string;
  logradouro?: string;
}
