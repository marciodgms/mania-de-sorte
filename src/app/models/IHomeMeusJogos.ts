﻿import { IMeusJogos } from ".";
import { IConcurso } from "./ILotofacil";

export interface IHomeMeusJogos {
  ultimo: IConcurso;
  lista: [IMeusJogos][];
}
