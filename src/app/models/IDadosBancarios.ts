export interface IDadosBancarios {
  id?: number;
  nomeCompleto?: string;
  cpf?: string;
  banco?: string;
  agencia?: string;
  conta?: string;
  contaDigito?: string;
}
