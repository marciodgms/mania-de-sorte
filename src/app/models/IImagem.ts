export interface IImagem {
  img_id?: number;
  nome_arquivo?: string;
  hash_arquivo?: string;
  caminho_arquivo?: string;
}
