export interface IFechamento {
  id?: number;
  fechamentoTipoFK?: number;
  qtdNumeros?: number;
  qtdDezenas?: number;
  qtdFixas?: number;
  descricao?: string;
  condicao?: any;
  ativo?: boolean;
}
