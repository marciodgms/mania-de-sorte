﻿export interface IConcurso {
  numero_concurso: number;
  data: Date;
  dezenas: string[];
}

export interface IProximoConcurso {
  data: Date;
  valor_estimado: number;
}

export interface ILotofacil {
  ultimo: IConcurso;
  proximo: IProximoConcurso;
}
