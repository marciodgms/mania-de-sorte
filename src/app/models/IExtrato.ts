import { IDadosBancarios } from "./IDadosBancarios";
import { ICarteira } from "./ICarteira";
import { ITransacao } from "./ITransacao";

export interface IExtrato {
  carteira?: ICarteira;
  dadosBancarios?: IDadosBancarios;
  transacoes?: ITransacao[];
}
