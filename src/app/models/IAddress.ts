export interface IAddress {
  idUsuario?: number;
  endereco?: string;
  uf?: string;
  cidade?: string;
  logradouro?: string;
  numero?: string;
  cep?: string;
}
