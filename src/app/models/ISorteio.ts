export interface ISorteio{
    id?: number;
    resultadoFk?: number;
    numero?: string;
    valor?: number;
    dataSorteio?: Date;
}