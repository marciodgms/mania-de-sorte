export interface IJogoModel {
  id?: number;
  numeros?: string[];
  acertos?: number;
}
