import { IJogo } from "./IJogo";
import { IBolao } from "./IBolao";

export interface IJogos{
    jogo?: IJogo;
    bolao?: IBolao;
}