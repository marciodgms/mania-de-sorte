import { Injectable } from '@angular/core';
import { HttpClientService } from '../http-client.service';
import { IPagamento } from '../models/IPagamento';
import { IPagamentoErro } from '../models/IPagamentoErro';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class PagamentoService {
  url = "api/pagamento";
  constructor( private _httpClientService: HttpClientService) { }
  
  public getAutentica(): Observable<any> {
    return this._httpClientService
      .get(this.url + "/autentica"); 
  }

  public getPagamentos(): Observable<any> {
    return this._httpClientService.get<IPagamento[]>(this.url);
  }

  public getPagamentoVigente(): Observable<any> {
    return this._httpClientService.get<IPagamento>(this.url + "/vigente");
  }

  public setConfirmacao(pagamento: IPagamento): Observable<any> {
    return this._httpClientService
      .post(this.url + "/confirmacao", pagamento); 
  }

  public setErro(pagamentoErro: IPagamentoErro): Observable<any> {
    return this._httpClientService
      .post(this.url + "/erro", pagamentoErro); 
  }
}
