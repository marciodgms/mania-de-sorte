import { Component, OnInit } from '@angular/core';
import { ToastrService } from "ngx-toastr";
import { NgZone } from '@angular/core';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

import { PagamentoService } from './pagamento.service';
import { IPagamento } from '../models/IPagamento';
import { IPagamentoErro } from '../models/IPagamentoErro';

declare var PagSeguroLightbox: any;

@Component({
  selector: 'app-pagamento',
  templateUrl: './pagamento.component.html',
  styleUrls: ['./pagamento.component.less']
})
export class PagamentoComponent implements OnInit {
  vigente: boolean = true;
  pagamentos: IPagamento[];
  pagamentoModel: IPagamento = {};
  pagamentoErroModel: IPagamentoErro = {};
  hashCode: any;
  dtOptions: DataTables.Settings =
    {
      searchPlaceholder: {
        caseInsensitive: true,
        searchPlaceholder: 'Pesquisar...'
      },
      language: {
        searchPlaceholder: 'Pesquisar...',
        search: '',
        loadingRecords: 'Carregando registros',
        processing: 'Processando',
        emptyTable: 'Nenhum registro foi encotrado',
        zeroRecords: 'Não encontrado, tente novamente',
        paginate: {
          first: 'Primeiro',
          last: 'Ultimo',
          next: 'Próximo',
          previous: 'Anterior'
        },
        aria: {
          paginate: {
            first: 'Primeiro',
            last: 'Ultimo',
            next: 'Próximo',
            previous: 'Anterior'
          },
          sortAscending: 'Ascendente',
          sortDescending: 'Descendente',
        },
        lengthMenu: '_MENU_',
        info: 'Página _PAGE_ de _PAGES_',
        infoFiltered: '(filtrado de _MAX_ de registros)'
      },
    };
  dtTrigger = null;

  constructor(
    private _route: Router,
    private pagamentoService: PagamentoService,
    public zone: NgZone,
    private _toastrService: ToastrService) {
  }

  ngOnInit() {
    this.carregaPagamentos();
    this.carregaPagamentoVigente();
  }

  carregaPagamentos() {
    this.pagamentoService
      .getPagamentos()
      .subscribe(response => {
        this.pagamentos = response;
        if (response.length == 0) {
          this.createSession();
        }
      });
  }

  carregaPagamentoVigente() {
    this.pagamentoService
      .getPagamentoVigente()
      .subscribe(response => {
        this.vigente = response != null;
      });
  }

  createSession() {
    this.pagamentoService
      .getAutentica()
      .subscribe(response => {
        this.hashCode = response.token;
        this.abrirPagseguro(response.redirect);

      }, error => {
        this._toastrService.error(
          error.json() || "Erro de autorização, por favor efetue o login novamente."
        );
      });
  }

  efetuarPagamento() {
    this.createSession();
  }

  abrirPagseguro(redirect) {
    if (typeof PagSeguroLightbox == 'function') {
      var isOpenLightbox = PagSeguroLightbox(this.hashCode, {
        success: (transactionCode) => {
          this.confirmePagApp(transactionCode)
        },
        abort: (mensagem) => {
          this.erroPagApp(mensagem)
        }
      });

      if (!isOpenLightbox) {
        location.href = redirect;
      }
    } else {
      setTimeout(() => {
        this.abrirPagseguro(redirect);
      }, 300);
    }
  }

  confirmePagApp(transactionCode) {
    this.pagamentoModel.codigoAutenticacao = this.hashCode;
    this.pagamentoModel.codigoConfirmacao = transactionCode;

    this.pagamentoService
      .setConfirmacao(this.pagamentoModel)
      .subscribe(response => {
        this._route.navigate(["home"]);
      }, error => {
        this._toastrService.error(
          error.json() || "Erro ao salvar os detalhes do pagamento."
        );
      });
  }

  erroPagApp(mensagem) {
    this.pagamentoErroModel.CodigoAutenticacao = this.hashCode;
    this.pagamentoErroModel.Mensagem = mensagem;

    this.pagamentoService
      .setErro(this.pagamentoErroModel)
      .subscribe(response => {
        window.location.reload();
      }, error => {
        this._toastrService.error(
          error.json() || "Erro ao salvar os detalhes do erro no pagamento."
        );
      });
  }
}
