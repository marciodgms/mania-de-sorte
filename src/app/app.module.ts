import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { PagamentoComponent } from './pagamento/pagamento.component';
import { HomeComponent } from './home/home.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CadastroComponent } from './cadastro/cadastro.component';
import { RecuperarSenhaComponent } from './login/recuperar-senha/recuperar-senha.component';
import { GerarFechamentoComponent } from './gerar-fechamento/gerar-fechamento.component';
import { UsuarioDetalhesComponent } from './usuario/usuario-detalhes/usuario-detalhes.component';
import { HttpModule } from '@angular/http';
import { BolaoListarComponent } from './boloes/bolao-listar/bolao-listar.component';
import {
  RecaptchaModule,
  RECAPTCHA_SETTINGS,
  RecaptchaSettings,
} from 'ng-recaptcha';
import { UsuarioListarComponent } from './usuario/usuario-listar/usuario-listar.component';
import { DataTablesModule } from 'angular-datatables';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { MeusJogosComponent } from './meus-jogos/meus-jogos.component';

import { NgXCreditCardsModule } from 'ngx2-credit-cards';
import { TutoriaisComponent } from './tutoriais/tutoriais/tutoriais.component';

import { SlickCarouselModule } from 'ngx-slick-carousel';
import { CoreModule } from './core.module';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import localePtExtra from '@angular/common/locales/extra/pt';

registerLocaleData(localePt, 'pt-BR', localePtExtra);
import { NgxMaskModule } from 'ngx-mask';
import { BolaoCriarComponent } from './boloes/bolao-criar/bolao-criar.component';
import { PremioSiteComponent } from './premio-site/premio-site.component';
import { NgxPrintModule } from 'ngx-print';
import { NgxYoutubePlayerModule } from 'ngx-youtube-player';
import { TutorialCriarComponent } from './tutoriais/tutorial-criar/tutorial-criar.component';
import { SaquesListarComponent } from './saques/saques-listar/saques-listar.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DetalhesJogoComponent } from './detalhes-jogo/detalhes-jogo.component';

import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    PagamentoComponent,
    HomeComponent,
    CadastroComponent,
    RecuperarSenhaComponent,
    GerarFechamentoComponent,
    UsuarioDetalhesComponent,
    BolaoListarComponent,
    UsuarioListarComponent,
    SaquesListarComponent,
    TutoriaisComponent,
    TutorialCriarComponent,
    MeusJogosComponent,
    BolaoCriarComponent,
    PremioSiteComponent,
    DetalhesJogoComponent,
    // LoaderComponent
  ],
  imports: [
    NgXCreditCardsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    RecaptchaModule,
    DataTablesModule.forRoot(),
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),
    SlickCarouselModule,
    NgxPrintModule,
    CoreModule,
    NgxMaskModule.forRoot(),
    NgxYoutubePlayerModule.forRoot(),
    NgxSpinnerModule,
    // NgXCreditCardsModule.forRoot()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [RouterModule],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },
    {
      provide: LOCALE_ID,
      useValue: 'pt-BR',
    },
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: {
        siteKey: environment.recaptchaSiteKey,
      } as RecaptchaSettings,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
