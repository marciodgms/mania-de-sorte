import { Component, OnInit } from '@angular/core';
import { ILogin } from 'src/app/models/ILogin';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';
import { LoaderService } from 'src/app/loader/loader.service';

@Component({
  selector: 'app-recuperar-senha',
  templateUrl: './recuperar-senha.component.html',
  styleUrls: ['./recuperar-senha.component.less']
})
export class RecuperarSenhaComponent implements OnInit {
  loginModel: ILogin = {};

  constructor(private _route: Router, private _loginService: LoginService, private _loaderService: LoaderService) { }

  ngOnInit() {
  }
  recuperar(){
    this._loginService.recuperarSenha(this.loginModel)
    .finally(() => {
      this._loaderService.hide();
    })
    .subscribe(x => x);
  }
}
