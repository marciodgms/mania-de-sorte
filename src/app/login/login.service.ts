import { Injectable } from '@angular/core';
import { HttpClientService } from '../http-client.service';
import { ILogin } from '../models/ILogin';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url = "api/autenticacao";
  constructor( private _httpClientService: HttpClientService) { }
  
  public logar(login: ILogin): Observable<any> {
    return this._httpClientService
      .post( this.url, login)
      .map((response) => {
        return response;
      });
  }
  public recuperarSenha(login: ILogin): Observable<any> {
    return this._httpClientService
      .post( this.url + '/recuperar', login)
      .map((response) => {
        return response;
      });
  }
}
