import { Component, OnInit, InjectionToken } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { ILogin } from '../models/ILogin';
import { IToken } from '../models/IToken';
import { LoaderService } from '../loader/loader.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  loginModel: ILogin = {};
  captchaRealizado = false;
  mostraMsg = false;
  errorMessage: any;
  constructor(private _route: Router, private _loginService: LoginService, private _loaderService: LoaderService) { }

  ngOnInit() {
    localStorage.clear();
  }
  login() {
    if (this.captchaRealizado) {
      this.mostraMsg = false;
      this._loginService.logar(this.loginModel)
        .finally(() => {
          this._loaderService.hide();
        })
        .subscribe(x => {
            let token = x as IToken;
            localStorage.setItem("currentUser", token.acessToken);
            this._route.navigate(["home"]);
        },err=>{
         this.errorMessage = (err.json().message);
        });
    }
    else {
      this.mostraMsg = true;
    }
  }
  cadastro() {
    this._route.navigate(["cadastro"]);
  }
  recuperar() {
    this._route.navigate(["recuperar-senha"]);
  }
  resolved(token: string) {
    this.captchaRealizado = !!token;
  }
}
