import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import { LoaderService } from './loader.service';
import {LoaderState} from './LoaderState';
import { DOCUMENT } from '@angular/platform-browser';
@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.less']
})
export class LoaderComponent implements OnInit {

  show = false;
  private subscription: Subscription;
  constructor(private _loaderService: LoaderService,
    @Inject(DOCUMENT) private document: Document) { }

  ngOnInit() {
    this.subscription = this._loaderService.loaderState
    .subscribe((state :LoaderState) => {
      this.show = state.show;
      if(this.show){
        this.document.body.classList.add('loader-show');
      }
      else{
        this.document.body.classList.remove('loader-show');
      }
    });
  }
  ngOnDestroy(){
    this.subscription.unsubscribe();
    
    if(this.show){
      this.document.body.classList.add('loader-show');
    }
    else{
      this.document.body.classList.remove('loader-show');
    }
  }

}
