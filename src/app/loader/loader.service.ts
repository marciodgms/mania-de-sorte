import { Injectable } from '@angular/core';
import { LoaderState } from './LoaderState';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  private loaderSubject = new Subject<LoaderState>();
  static loaderState : Observable<LoaderState>;
  loaderState = this.loaderSubject.asObservable();
  constructor() { }
  show(){
    this.loaderSubject.next(<LoaderState>{show: true});
    return true;
  }
  hide(){
    this.loaderSubject.next(<LoaderState>{show:false});
    return false;
  }
}
