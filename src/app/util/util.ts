﻿export const objectToFormData = (data: any) => {
  const formData = new FormData();
  const keys = Object.keys(data);

  keys.forEach((key) => {
    if (data[key]) {
      if (typeof data[key] !== 'object' && !Array.isArray(data[key])) {
        formData.append(key, data[key]);
      } else {
        if (typeof data[key] === 'object' && !Array.isArray(data[key])) {
          const prefix = key;
          const newKeys = Object.keys(data[key]);

          newKeys.forEach((nkey) => {
            if (isNumber(nkey)) {
              formData.append(key, data[key][nkey]);
            } else {
              formData.append(prefix + '.' + nkey, data[key][nkey]);
            }
          });
        } else if (Array.isArray(data[key])) {
          data[key].forEach((arData: any, index: any) => {
            if (typeof arData !== 'object') {
              formData.append(key, arData);
            } else {
              if (arData.hasOwnProperty('path')) {
                formData.append(key, arData);
              } else {
                const objArKeys = Object.keys(arData);

                objArKeys.forEach((objArkey) => {
                  formData.append(
                    `${key}[${index}].${objArkey}`,
                    arData[objArkey]
                  );
                });
              }
            }
          });
        }
      }
    }
  });

  return formData;
};

export const isNumber = (n: any) => {
  return !isNaN(parseFloat(n)) && !isNaN(n - 0);
};

export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
