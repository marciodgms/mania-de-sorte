import { Injectable } from "@angular/core";
import { HttpClientService } from "../http-client.service";
import { ILotofacil } from "../models";

@Injectable({
  providedIn: "root",
})
export class HomeService {
  private apiUrl = "api/home";

  constructor(public httpClientService: HttpClientService) {}

  obterJogo(numeroConcurso: number) {
    return this.httpClientService.get<ILotofacil>(
      `${this.apiUrl}/jogo/${numeroConcurso || ""}`
    );
  }
}
