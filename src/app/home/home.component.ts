import { Component, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { LoaderService } from "../loader/loader.service";
import { MeusJogosService } from "../meus-jogos/meus-jogos.service";
import { HomeService } from "./home.service";
import { IConcurso, IMeusJogos, IProximoConcurso } from "../models";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.less"],
})
export class HomeComponent implements OnInit {
  private jogosInexistentes = [1998, 1999];

  ultimo: IConcurso = {
    numero_concurso: 0,
    data: null,
    dezenas: Array(15).fill("0"),
  };
  proximo: IProximoConcurso = {
    data: null,
    valor_estimado: 0,
  };

  meusJogos: IMeusJogos[];
  valorGanho = 0;

  constructor(
    private _homeService: HomeService,
    private _meusJogosService: MeusJogosService,
    private _loaderService: LoaderService,
    private _toastrService: ToastrService
  ) {}

  ngOnInit() {
    this.obterConcurso();
    this.carregarDados();
  }

  carregarDados() {
    this._meusJogosService
      .obterMeusJogos()
      .finally(() => this._loaderService.hide())
      .subscribe((meusJogos) => {
        this.meusJogos = meusJogos;
        this.quantoGanhei();
      });
  }

  quantoGanhei() {
    if (this.meusJogos && this.meusJogos.length) {
      const nroUltimoConcurso = Math.max(
        ...this.meusJogos.map((j) => j.concurso)
      );

      const meuJogo = this.meusJogos.find(
        (j) => j.concurso === nroUltimoConcurso
      );

      this.valorGanho = meuJogo.premioRecebido;
    }
  }

  obterConcurso(idConcurso?: number) {
    this._homeService
      .obterJogo(idConcurso)
      .finally(() => this._loaderService.hide())
      .subscribe(
        ({ ultimo, proximo }) => {
          if (ultimo.numero_concurso) {
            this.ultimo = ultimo;
            this.proximo = proximo;
          }
        },
        (error) => {
          if (
            error.status === 404 &&
            !this.jogosInexistentes.includes(idConcurso)
          ) {
            this._toastrService.info("Este é o último concurso");
          } else {
            console.error(
              `Erro obtendo concurso ${idConcurso}`,
              error.message || error._body,
              error
            );
          }
        }
      );
  }

  previous() {
    this.obterConcurso(this.ultimo.numero_concurso - 1);
  }

  next() {
    this.obterConcurso(this.ultimo.numero_concurso + 1);
  }
}
