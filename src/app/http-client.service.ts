import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import "rxjs/add/operator/finally";
import { LoaderService } from "./loader/loader.service";
import { map, finalize } from "rxjs/operators";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class HttpClientService {
  url = environment.apiUrl;

  autorizationHeader = true;
  isLogged = false;

  constructor(private http: Http, private loaderService: LoaderService) {}

  createAuthorizationHeader(headers: Headers) {
    if (!this.autorizationHeader) {
      return;
    }

    const currentUser = localStorage.getItem("currentUser");

    if (currentUser) {
      headers.append("Authorization", `Bearer ${currentUser}`);
    }
  }

  verifyLogged() {
    const currentUser = localStorage.getItem("currentUser");

    this.isLogged = !!currentUser;

    return this.isLogged;
  }

  createApplicationJson(headers: Headers) {
    // headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    // headers.append('Access-Control-Allow-Origin', '*')
    // headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
  }

  getHeaders() {
    const headers = new Headers();

    this.createApplicationJson(headers);
    this.createAuthorizationHeader(headers);

    return headers;
  }

  get<T>(url): Observable<T> {
    this.loaderService.show();

    return this.http
      .get(this.url + url, {
        headers: this.getHeaders(),
      })
      .pipe(
        map((d: any) => {
          try {
            return d.json();
          } catch (e) {
            return `[nOK] ${e.message}`;
          }
        }),
        finalize(() => this.loaderService.hide())
      );
  }

  externalGet(url): any {
    this.loaderService.show();

    return this.http
      .get(url, {
        headers: this.getHeaders(),
      })
      .pipe(
        map((d: any) => {
          try {
            return d.json();
          } catch (e) {
            return `[nOK] ${e.message}`;
          }
        }),
        finalize(() => this.loaderService.hide())
      );
  }

  post(url, data) {
    this.loaderService.show();

    return this.http
      .post(this.url + url, data, {
        headers: this.getHeaders(),
      })
      .pipe(
        map((d: any) => {
          try {
            return d.json();
          } catch (e) {
            return `[nOK] ${e.message}`;
          }
        }),
        finalize(() => this.loaderService.hide())
      );
  }

  put(url, data?) {
    this.loaderService.show();

    return this.http
      .put(this.url + url, data, {
        headers: this.getHeaders(),
      })
      .pipe(
        map((d: any) => {
          try {
            return d.json();
          } catch (e) {
            return `[nOK] ${e.message}`;
          }
        }),
        finalize(() => this.loaderService.hide())
      );
  }

  delete(url) {
    this.loaderService.show();

    return this.http
      .delete(this.url + url, {
        headers: this.getHeaders(),
      })
      .pipe(
        map((d: any) => d._body),
        finalize(() => this.loaderService.hide())
      );
  }
}
